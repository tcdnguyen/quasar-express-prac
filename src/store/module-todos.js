import Vue from 'vue';

const state = {
    todos: [
        {
            name: "do 1",
            completed: false
        },
        {
            name: "done 1",
            completed: false
        },
        {
            name: "don't 1",
            completed: true
        }
    ]
}
const mutations = {
    addTodo(state, payload) {
        state.todos.push(payload);
    },
    changeTodo(state, payload) {
        let { idx, todo } = payload;
        Vue.set(state.todos, idx, todo);
    },
    removeTodo(state, idx) {
        state.todos.splice(idx, 1);
    }
}
const actions = {
    addTodo({ commit }, payload) {
        commit("addTodo", payload);
    },
    changeTodo({ commit }, payload) {
        commit("changeTodo", payload);
    },
    removeTodo({commit}, idx) {
        commit("removeTodo", idx);
    }
}
const getters = {

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}